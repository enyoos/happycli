# Project Overview
Happycli is a library to make your System.out.println(...) more colorful, more happy.
Also, it enables you to make cool progress bars.
Thanks to the builder pattern it highly extensible.

## Install
``mvn install``.
