package com.happycli.java;

import org.junit.Test;

import com.happycli.java.PaintOptions;
import static com.happycli.java.Utils.*;

import java.io.IOException;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private static final String DELIMETER = "---------------------";
    private static String output = "";
    private static final String content   = "Hello, World";
    private static TextureBuilder tb      =  new TextureBuilder().content(content);


    // @Test
    // public void isDefaultWork( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[DEFAULT, NO ADD-ONS]");

    //     String output = new TextureBuilder().content(content).build();
    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isFgWorking ( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[FOREGROUND, ALL PRIMARY COLORS]");

    //     output = tb.foreground(PaintOptions.BLACK).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.BLUE).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.WHITE).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.YELLOW).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.GREEN).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.RED).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.CYAN).build();
    //     System.out.println(output);
    //     output = tb.foreground(PaintOptions.MAGENTA).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isBgWorking( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[BG ALL PRIMARY COLORS]");
        
    //     output = tb.background(PaintOptions.BLACK).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.BLUE).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.WHITE).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.YELLOW).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.GREEN).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.RED).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.CYAN).build();
    //     System.out.println(output);
    //     output = tb.background(PaintOptions.MAGENTA).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
 
    // }

    // @Test
    // public void isFaintWorking ()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[FAINT]");

    //     output = tb.faint(true).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isBlinkWorking ( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[BLINK]");

    //     output = tb.blink(true).build();
    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isBoldWorking ( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[BOLD]");
    //     output = tb.bold(true).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isUnderlineWorking ()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[UNDERLINE]");

    //     String output = tb.underline(true).build();
    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isResetWorking ( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[RESET]");

    //     output = tb.foreground(PaintOptions.BLUE).build();
        
    //     System.out.println(output);
    //     System.out.println(DELIMETER);

    // }


    // @Test
    // public void isRGBWorkingFG ( )
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[RGB FG]");

    //     int[] vec = {201, 186, 175};
    //     output = tb.foreground(PaintOptions.fromRGB(vec)).build();
    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test
    // public void isRGBWorkingBG ()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[RGB BG]");
    //     int[] vec = {201, 186, 175};
    //     output = tb.background(PaintOptions.fromRGB(vec)).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }

    // @Test 
    // public void isHEXWorkingBG()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[HEX BG]");
    //     String hex = "#FFFFFF";
    //     output = tb.background(PaintOptions.fromHex(hex)).build();

    //     System.out.println ( output );
    //     System.out.println(DELIMETER);
    // }

    // @Test 
    // public void isHEXWorkingFG()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[HEX FG]");
    //     String hex = "#FFFFFF";
    //     output = tb.foreground(PaintOptions.fromHex(hex)).build();

    //     System.out.println ( output );
    //     System.out.println(DELIMETER);
    // }

    // @Test 
    // public void isItalicWorking()
    // {
    //     tb.clear();
    //     tb.content(content);
    //     System.out.println("[ITALIC]");
    //     output = tb.italic(true).content(content).build();

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }


    // @Test 
    // public void toHex()
    // {
    //     System.out.println("[FROM_HEX(), PAINT_OPTIONS]");
    //     String hex = "#FFA500";
    //     output = PaintOptions.fromHex(hex);

    //     System.out.println(output);
    //     System.out.println(DELIMETER);
    // }


    // testing the new feature.
    // so here's the OOP plan : 
    // the user shall supply a dynamic value ( manual )
    // let's try to handle http download time tracking
    // @Test
    // public void loadBar( ) throws Exception
    // {
    //     // the total ( when the loading ends )
    //     final int total = 1000;

    //     String out = "";
    //     long wait = 10;
    //     float status = 0f;

    //     for ( float i = 0 ; i < total; i ++ )
    //     {
    //         status =  ( i / total ) * 100 ;
    //         out = String.format ( "[INFO] status : %f %% \r", status);

    //         // must be printed without the new line...
    //         System.out.print(out);

    //         // with the thread pause it makes it more natural ?
    //         Thread.sleep(wait);
    //     }
    // }

   // @Test
   // public void trackTimeDownLoad() throws Exception
   // {

   //     TextureBuilder tb = new TextureBuilder();
   //     tb.foreground ( PaintOptions.RED );
   //     tb.underline  (true)              ;

   //     String url_ = "https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt";
   //     URL url = new URL ( url_ );

   //     Pattern pattern = Pattern.DEF;

   //     Tracker.setTextureBuilder ( tb );
   //     
   //     try {
   //         Tracker.downloadUsingStream(url, "test.txt", pattern );
   //     } catch (IOException e) {
   //         e.printStackTrace();
   //     }
   // }
   //
    
    
    // @Test
    // public void testListening ()
    // {
	    // Integer dynamicValue = new Integer (4);
	    // System.out.println( dynamicValue );
	    // System.out.println( "init hash > " + dynamicValue );

	    // dynamicValue.add ( 1 );

	    // System.out.println( "
    // }

    
    
    // @Test
    // public void clear()
    // {
    //     String out = "Hello, World";
    //     int n = 5;


    //     System.out.print(out);
    //     System.out.print( constructCancel(n) );
    //     System.out.print(out);

    //     System.out.print( constructCancel(n) );
    // }
}

