package com.happycli.java;

public enum Pattern 
{
    HASH ( "#" ),
    CROSS( "X" ),
    RATIO( "%" ),
    DEF  ( " " ),
    LINE ( "-" );

    private final String value;
    private Pattern ( String value ) { this.value = value; }
    public String value ( ) { return this.value; }
}
