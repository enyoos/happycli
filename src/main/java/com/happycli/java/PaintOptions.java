package com.happycli.java;

import java.awt.Color;
import java.awt.Paint;

/**
 * PaintOptions class contains the main options for
 * string coloring (  mainly stringified integers for colors code ).
 * 
 * It also has some utilities method for coloring.
 * 
 * @since 1.0
 */
public final class PaintOptions {
    
    public final static String BLACK = "0" ;
    public final static String RED   = "1" ;
    public final static String GREEN = "2" ;
    public final static String YELLOW = "3";
    public final static String BLUE   = "4";
    public final static String MAGENTA = "5";
    public final static String CYAN    = "6";
    public final static String WHITE = "7" ;

    /**
     * takes 3 values ( r, g, b, respectively )
     * and return the specified string.
     * Take a look to the example below.
     * 
     * <br>
     * <pre>
     * // I want to color the output (set the fg ) with rgb( 255, 255, 255 )
     * String white           = "White";
     * String specifiedString = PaintOptions.fromRGB ( 255, 255, 255 );
     * white                  = new TextureBuilder ( ).content ( white ).foreground( specifiedString )...;
     * </pre>
     * 
     * @param r int
     * @param g int
     * @param b int
     * @return String
     */
    public static String fromRGB( int r, int g, int b )
    {
        return String.format("8;2;%d;%d;%d", r, g, b);
    }

   
    /**
     * This method takes any class implementing the Paint interface
     * extract the rgb values and calls internally the fromRGB method {@link PaintOptions#fromRGB(int,int,int)}  
     * 
     * <br>
     * <pre>
     * Paint clr = Color.BLACK;
     * String black = "Black";
     * String specifiedString = PaintOptions.fromColor ( clr );
     * black = new TextureBuilder( ).content( black ).foreground( specifiedString )...;
     * </pre>
     * 
     * @param clr Paint
     * @return String
     * 
    */
    public static String fromColor( Paint clr )
    {
        int r = ( ( Color ) clr).getRed();
        int g = ((Color) clr).getGreen();
        int b = ((Color) clr).getBlue();

        return fromRGB(r, g, b);
    }

    /**
     * deconstructs the rgb array, and calls internally {@link PaintOptions#fromRGB(int, int, int)}
     * 
     * <br>
     * <pre>
     * String white           = "White";
     * int[]  vec             = {255, 255, 255};
     * String specifiedString = PaintOptions.fromRGB ( vec );
     * white                  = new TextureBuilder ( ).content ( white ).foreground( specifiedString )...;
     * </pre>
     * 
     * @param arr int[]
     * @return String
     */
    public static String fromRGB( int[] arr )
    {
        return fromRGB(arr[0], arr[1], arr[2]);
    }

    /**
     * extract the r,g,b values out of the hex.
     * 
     * Calls internally {@link PaintOptions#fromRGB(int, int, int)}
     * 
     * <br>
     * <pre>
     * String white           = "White";
     * String hex             = "#FFFFFF";
     * String specifiedString = PaintOptions.fromHex( hex );
     * white                  = new TextureBuilder ( ).content ( white ).foreground( specifiedString )...;
     * </pre>
     * 
     * @param hex String
     * @return String
     */
    public static String fromHex ( String hex )
    {
        int radix = 16;
        if ( hex.contains("#") ) { hex = hex.substring(1, hex.length()); }

        int r = Integer.valueOf(hex.substring(0, 2), radix);
        int g = Integer.valueOf(hex.substring(2, 4), radix);
        int b = Integer.valueOf(hex.substring(4, 6), radix);

        return fromRGB(r, g, b);
    }

    /**
     * private constructor, nothing to see.
     */
    private PaintOptions(){ }
}
