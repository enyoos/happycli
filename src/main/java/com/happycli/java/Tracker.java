package com.happycli.java;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

public final class Tracker {

    private static TextureBuilder tb = null;
    private static int RECT_SIZE = 50;
    private Tracker() { }

    public static void setTextureBuilder ( TextureBuilder tb ) { Tracker.tb = tb; }

	public static void downloadUsingStream(URL url, String filename, Pattern pattern ) throws IOException, InterruptedException{

        // get the headers response
	final int TOTAL_FRAG_SIZE = Integer.parseInt( getResponseHeader(url).get("Content-Length" ).get(0));
	BufferedInputStream bis   = new BufferedInputStream(url.openStream());
	FileOutputStream fis      = new FileOutputStream(filename);

	    switch ( pattern )
	    {
		    // is there any better way write this ?
		    case LINE :
			    stdOutWithPattern( TOTAL_FRAG_SIZE, bis, fis, pattern );
			    break;

		    case RATIO:
			    stdOutWithPattern( TOTAL_FRAG_SIZE, bis, fis, pattern );
			    break;

		    case CROSS:
			    stdOutWithPattern( TOTAL_FRAG_SIZE, bis, fis, pattern );
			    break;
			    
		    case HASH:
			    stdOutWithPattern( TOTAL_FRAG_SIZE, bis, fis, pattern );
			    break;

		    case DEF:
			    defaultStdOut( TOTAL_FRAG_SIZE, bis, fis );
			    break;

		    default:
			    System.out.println("[ERROR] This pattern type is not supported");
			    break;
	    }

    }

    public static void downloadUsingStream( URL url, String filename ) throws IOException, InterruptedException
    {
	    Pattern defaultPattern = Pattern.DEF;
	    downloadUsingStream ( url, filename, defaultPattern );
    }

    private static void defaultStdOut ( final int size, BufferedInputStream bis, FileOutputStream fis ) throws IOException, InterruptedException
    {
        int bytesRead = 0;
        byte[] buffer = new byte[1024];
        int count     =0;
        
        long writeTimeBegin = 0;
        long writeTimeEnd   = 0;

        long readTimeInit= 0;
        long readTimeEnd = 0;

        // delta for Reading
        // delta for Writing
        long deltaR    = 0;
        long deltaW    = 0;
	String content = "";
        boolean done   = true;

        // if the read is succ, then return the number of byte read ( 1024 )
	System.out.println( "" );
        while( done )
        {

            readTimeInit  = System.currentTimeMillis();
            done = (count = bis.read(buffer,0,1024) ) != -1;
            readTimeEnd   = System.currentTimeMillis();

            // TODO : change the var name accordingly
            // TODO : FIX THE INFINITY STUFF
            if ( !done ) { break; }

            bytesRead += count;

            writeTimeBegin = System.currentTimeMillis();
            fis.write(buffer, 0, count);
            writeTimeEnd   = System.currentTimeMillis();



            deltaR = readTimeEnd - readTimeInit; 
            deltaW = writeTimeEnd - writeTimeBegin;

            // if I want a percentage, we must the size of the actual file before even downlaoding it
	    content = String.format ( "[INFO] downloading... %.02f bytes %% [speed conn : %.02f bytes/s]\r",
				    ( double ) getPercentInstall( 
					    ( float ) (bytesRead * 1.0),
					   	size 
					    ) ,
				    ( double )getVelocityConn(deltaR, deltaW));

	    if ( tb != null ){
		    tb.content( content );
		    content = tb.build() ;
		    System.out.print( content );
	    }
	    else System.out.print( content );

        }

        System.out.println ( content );

        fis.close();
        bis.close();

    }

    private static String constructPaddedString( int rs )
    {
	    String acc = "";
	    for ( int i = 0 ; i < rs; i++ ) { acc   += " "; }

	    return acc;
    }
    
    private static void setDefaultSize ( int size ) { RECT_SIZE = size; }

    private static void stdOutWithPattern( final int size, BufferedInputStream bis, FileOutputStream fis, Pattern pattern ) throws IOException, InterruptedException
    {
	    // this is the rect size
	    // [XXXXXXXXXXXXXXXX] ( 20 space bar char )
	    // char are the same width (hopefully)
	    // this can be modified ?
	    final int rectSize         = 100                               ;
	    String acc                 = constructPaddedString ( rectSize );
            int   rectSizeCpy          = rectSize                          ;
	    
	    // each time we get to this goal, then we draw one pattern
	    final int goalForOnePatternDrawn = size / rectSize;

	    // this temp count is to check if we've reached the goal for one pattern drawn.
	    int tempCount              = 0;

        int bytesRead = 0;
        byte[] buffer = new byte[1024];
        int count     = 0;
        
        long writeTimeBegin = 0;
        long writeTimeEnd   = 0;

        long readTimeInit= 0;
        long readTimeEnd = 0;

        // delta for Reading
        // delta for Writing
        long deltaR    = 0;
        long deltaW    = 0;
        boolean done   = true;
	String content = "";

         // if the read is succ, then return the number of byte read ( 1024 )
	 System.out.println ( "" );
        while( done )
        {

            readTimeInit  = System.currentTimeMillis()     ;
            done = (count = bis.read(buffer,0,1024) ) != -1;
            readTimeEnd   = System.currentTimeMillis()     ;

            if ( !done ) { break; }

            bytesRead += count;
	    tempCount += count;

            writeTimeBegin = System.currentTimeMillis();
            fis.write(buffer, 0, count);
            writeTimeEnd   = System.currentTimeMillis();


            deltaR = readTimeEnd  - readTimeInit  ; 
            deltaW = writeTimeEnd - writeTimeBegin;

            // if I want a percentage, we must the size of the actual file before even downlaoding it
	    if ( tempCount >= goalForOnePatternDrawn ) { 
		    acc       = replaceWithPattern ( acc, pattern, rectSize - rectSizeCpy );
		    rectSizeCpy--;
		    tempCount = 0;
	    }

	    content = String.format ( "[INFO] dowloading ... [%s]\r", acc );

	    if ( tb != null ){
		    tb.content( content );
		    content = tb.build() ;
		    System.out.print( content );
	    }
	    else System.out.print( content );

        }

	System.out.println ( content );

        fis.close();
        bis.close();

    }

    private static String replaceWithPattern ( String content, Pattern pattern, int iter )
    {
	    String oldstr = " "                                     ;
	    String newstr = pattern.value( )                        ;

	    if ( iter == 0 ) { return content.replaceFirst ( oldstr , newstr ); }

	    content       = content.replaceFirst ( oldstr , newstr );

	    iter--;

	    return replaceWithPattern ( content, pattern, iter );
    }

    private static String constructPaddedPattern ( int size, Pattern pattern  )
    {

	    String acc = "";

	    for ( int i = 0; i < size ; i ++ ) { acc += pattern.value (); }

	    return acc;
    }

        private static Map<String, List<String>> getResponseHeader( URL url )
    {
        try {
            return url.openConnection().getHeaderFields();
        }
        catch ( IOException e )
        {
            System.err.println( "ERROR: couldn't open the connection : " + e );

            // the compiler is not intelligent enough to tell me that the return is "unreachable "?;
	    final int errCode = 1;
            System.exit(errCode);

            return null;

        }
    }

    private static float getPercentInstall ( float currentBytes, final int totalBytes ) { return ((( currentBytes) /totalBytes) * 100); }
    private static float getVelocityConn   ( long dR, long dW )
    {

        // usually the read time will be greater than the write time ( by intuition : local vs internet , local always win ) 
        long dT = dR - dW;
        if ( dT < 0 ) return 0; 

        // now all we need is to know the velocity of the connection.
        // we know that how many bytes we read ( which is always 1024 ).
        // we must know the time for read ops.
        // we should move the read op into the while loop
        // the velocity is in bytes/second

        return ( float ) 1024.0 / dT;
    }

}
