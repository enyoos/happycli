package com.happycli.java;


/**
 * Builds the texture for the cli coloring.
 * Using the builder pattern.
 * 
 * @author envy, envyoos
 * @since 1.0
 */
public class TextureBuilder {
    
    /**
     * the texture object.
     */
    private Texture texture = new Texture();

    /**
     * 
     * If you want the output to be faint, supply this
     * method with the boolean true. 
     * <br>
     * 
     * <pre>
     * new TextureBuilder().faint(true)...
     * </pre>
     * 
     * @param b boolean
     * @return self
     */
    public TextureBuilder faint(boolean  b)
    {
        this.texture.setIsFaint(b);
        return this;
    }

    public TextureBuilder italic( boolean b )
    {
        this.texture.setItalic(b);
        return this;
    }

    /**
     * 
     * If you want the output to blink, supply this
     * method with the boolean true
     * <br>
     * 
     * <pre>
     * new TextureBuilder().blink ( true )...
     * </pre>
     * 
     * @param b boolean
     * @return self
     */
    public TextureBuilder blink( boolean b )
    {
        this.texture.setIsBlinking(b);
        return this;
    }


    /**
     * If you want the output to be bold, supply this
     * method with the boolean true
     * <br>
     * 
     * <pre>
     * new TextureBuilder().bold( true )...
     * </pre>
     * 
     * @param b boolean
     * @return self
     */
    public TextureBuilder bold( boolean b )
    {
        this.texture.setIsBold(b);
        return this;
    }


    /**
     * Apply the specified foreground ( color of the text ) to the output.
     * Use {@link PaintOptions} in order to convert the hex, rgb, or Color
     * class to a valid string.
     * 
     * <br>
     * <pre>
     * int[] someColor = {1, 4, 66};
     * String validColor = PaintOptions.fromRGB( someColor[0], someColor[1], someColor[2] );
     * new TextureBuilder().foreground( validColor )...
     * </pre>
     * 
     * @param str foreground
     * @return self
     */
    public TextureBuilder foreground ( String str )
    {
        this.texture.setForeground( "3" + str);
        return this;
    }

    /**
     * Apply the specified background to the output.
     * Use {@link PaintOptions} in order to convert the hex, rgb, or Color
     * class to a valid string.
     * 
     * <br>
     * <pre>
     * Color clr = new Color.BLACK;
     * String validClr = PaintOptions.fromColor( clr );
     * new TextureBuilder().background( validClr )...
     * </pre>
     * 
     * @param str background
     * @return self
     */
    public TextureBuilder background( String str )
    {
        this.texture.setBackground("4" + str);
        return this;
    }

    /**
     * Apply the underline effect to the output.
     * 
     * @param t boolean
     * @return self
     */
    public TextureBuilder underline ( boolean t )
    {
        this.texture.setIsUnderlined(t);
        return this;
    }

    /**
     * Reset the effects for the next output.
     * By default, it is true.
     * 
     * <br>
     * <pre>
     * new TextureBuilder().( true )...
     * </pre>
     * 
     * @param t boolean
     * @return self
     */
    public TextureBuilder reset ( boolean t )
    {
        this.texture.setIsReset( t );
        return this;
    }

    /**
     * set the output.
     * 
     * <br>
     * <pre>
     * String stdout = "Hello world";
     * new TextureBuilder(  ).content ( stdout );
     * </pre>
     * 
     * @param content String
     * @return self
     */
    public TextureBuilder content ( String content ) 
    {
        this.texture.setContent(content);
        return this;
    }

    /**
     * Clears all the effects.
     * 
     * <br>
     * <pre>
     * static TextureBuilder tb = new TextureBuilder().content( "something" );
     * 
     * void main ( String ... args )
     * {
     *      setRandomEffects( tb );
     *      tb.clear();
     * }
     * </pre>
     * 
     */
    public void clear   ( )
    {
        this.texture.clearParams();
    }
    
    /**
     * Return the final output with the applied effects
     * 
     * @return String
    */

    public String build()
    {
        return this.texture.getOutputString();
    }

}