package com.happycli.java;

/**
 * Utils class is containing <p>helper methods</p>
 * and some important constants.
 */
public class Utils {

    /**
     * ESCAPE is the NULL char,
     */
    private static char ESCAPE = ( char ) 27;
    private static String BACK_SPACE = "\b";
    /**
     * CSI is equivalent to : NULL + '['
     * Every colored, or modified string, needs to start with
     * the CSI string.
     * 
     * To know more, read <a>https://chrisyeh96.github.io/2020/03/28/terminal-colors.html</a>
     */
    public static String CSI = ESCAPE + "[";
     
    // Utils method for the updating cli
    public static String constructCancel ( String out ) 
    {
        int length = out.length();
        String ret = "\b";
        for ( int i = 0 ; i < length; i ++ ) { ret += BACK_SPACE; }
        return ret;
    }

    public static String constructCancel ( int length )
    {
        String ret = "\b";
        for ( int i = 0 ; i < length; i ++ ) { ret += BACK_SPACE; }
        return ret;
    }

    /**
     * DELIMETER_PARAMS is useful for separating 
     * the different params ( effects ) to the output.
     */
    public static final char DELIMETER_PARAMS = ';';
    public static final String UNDERLINE_CODE = "4";
    public static final String ITALIC_CODE    = "3";
    public static final String BOLD_CODE      = "1";
    public static final String FAINT_CODE     = "2";
    public static final String RESET_CODE     = "0";
    public static final String BLINK_CODE     = "5";

}

