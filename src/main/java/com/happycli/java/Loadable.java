package com.happycli.java;

public interface Loadable
{
	// if we want to keep track of the progress
	// of a process, we need : initial amount ( could be anything ), final amount, and counter
	
	// init, last, counter, context ( the msg shown in the progress bar ).
	// public void load(final T init, final T last, T counter, String context ); 
	// public void update( T value );
}
