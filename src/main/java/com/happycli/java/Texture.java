package com.happycli.java;
import java.util.ArrayList;

/**
 * Texture class
 * handles the output.
 * 
 * @since 1.0
 * 
 */
public class Texture {

    /**
     * Default values
     */
    private static final boolean DEFAULT_RESET   = true;
    private static final String  DEFAULT_CONTENT = ""  ;


    /**
     * ArrayList holding all the specified effects.
     */
    private ArrayList<String> params; 
    /**
     * reset value, boolean, defaulted to 0.
     */
    private boolean reset = DEFAULT_RESET;
    /**
     * content, string, the output to cmd, terminal-emulator.
     */
    private String content= DEFAULT_CONTENT;

    /**
     * protected constructor, initializing the params ArrayList.
     */
    protected Texture() { this.params = new ArrayList<>();}

    /**
     * add specified background.
     * @param bg String
     */
    protected void setBackground( String bg )
    {
        this.params.add(bg);
    }

    /**
     * clears the input 
     */

    /**
     * apply the <em>italic</em> effect to the output string.
     * 
     * <br>
     * <pre>
     * String italic = "pizza";
     * String out    = new TextureBuilder().content ( italic ).italic(true)...;
     * </pre>
     * 
     * @param b boolean
     */
    protected void setItalic( boolean b )
    {
        if ( b ) this.params.add(Utils.ITALIC_CODE);
    }

    /**
     * add specified foreground.
     * @param fg String
     */
    protected void setForeground( String fg )
    {
        this.params.add(fg);
    }

    /**
     * toogle bold effect.
     * @param b boolean 
     */
    protected void setIsBold( boolean b )
    {
        if ( b ) this.params.add(Utils.BOLD_CODE);
    }

    /**
     * toogle faint effect.
     * @param f boolean 
     */
    protected void setIsFaint ( boolean b )
    {
        if ( b ) this.params.add( Utils.FAINT_CODE );
    }
        
    /**
     * toogle underline effect.
     * 
     * @param b boolean
     */
    protected void setIsUnderlined ( boolean b )
    {
        if ( b ) this.params.add( Utils.UNDERLINE_CODE);
    }

    /**
     * clears the params array ( including the content ).
     */
    protected void clearParams     ()
    {
        this.params.clear();
    }

    /**
     * toogle blink effect
     * @param b boolean
     */
    protected void setIsBlinking ( boolean b )
    {
        if ( b ) this.params.add( Utils.BLINK_CODE );
    }

    /**
     * toogle reset effect
     * @param b boolean
     */
    protected void setIsReset ( boolean b ) 
    {
        this.reset = b;
    }

    /**
     * sets the content of the texture object.
     * 
     * @param content String
     */
    protected void setContent ( String content ) { this.content = content;}

    /**
     * computes the final output with the applied effects.
     * 
     * @return String
     */
    protected String getOutputString()
    {

        String ret = Utils.CSI;
        String tt = "";
        String t = null;
        int l = this.params.size();

        for ( int i = 0 ; i < l ; i ++ )
        {
            t = this.params.get(i);
            if ( i == 0 ) ret += t;
            else {
                tt = Utils.DELIMETER_PARAMS + t;
                ret += tt;
            }
        }

        String end = null;
        if ( this.reset ) { end = "m" +  content + Utils.CSI + Utils.RESET_CODE + "m"; }
        else end = "m" + content;

        ret += end;
        return ret;

    }
}